import { combineReducers } from "redux";
//pulling out a redux about users
import userReducer from './user/userReducer';

//fixing the producer about users in combineReducers
const rootReducer = combineReducers({
    user: userReducer
})

export default rootReducer;