import {
    //we pull out the label of the beginning of the process of obtaining information about users
    //with server https://jsonplaceholder.typicode.com/users
    FETCH_USERS_REQUEST,
    //we pull out the label of successful receipt of user information from the specified server
    FETCH_USERS_SUCCESS,
    //we pull out the label of unsuccessful receipt of user information from the specified server
    FETCH_USERS_FAILURE
} from './userTypes'

//the initial state of the ongoing downloading of user information from the specified server
const initialState = {
    loading: false,
    users: [],
    error: ''
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        //the process of downloading user information from the specified server is underway
        //the execution of this process is signaled in the store using the logical loading flag
        case FETCH_USERS_REQUEST:
        {
            return {
                ...state,
                loading: true
            }
        } 
        //the process of downloading user information from the specified server has been successfully carried out
        //the received action.payload information is transferred to the store
        case FETCH_USERS_SUCCESS:
        {
            return {
                loading: false,
                users: action.payload,
                error: ''
            }
        }
        //the process of downloading user information from the specified server failed
        case FETCH_USERS_FAILURE:
        {
            return {
                loading: false,
                users: [],
                error: action.payload
            }
        }

        default:
        {
            return state;
        }
    }
}

export default reducer;