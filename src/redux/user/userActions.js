import axios from 'axios';

import { 
    //we pull out the label of the beginning of the process of obtaining information about users
    //with server https://jsonplaceholder.typicode.com/users
    FETCH_USERS_REQUEST, 
    //we pull out the label of successful receipt of user information from the specified server
    FETCH_USERS_SUCCESS, 
    //we pull out the label of unsuccessful receipt of user information from the specified server
    FETCH_USERS_FAILURE 
} from "./userTypes"

//a request begins to receive user information from the specified server
export const fetchUsersRequest = () => {
    return {
        type: FETCH_USERS_REQUEST
    }
}

//this function signals that, that the request to obtain user information
//from the specified server has been successfully completed and sends the received information to the store
export const fetchUsersSuccess = (users) => {
    return {
        type: FETCH_USERS_SUCCESS,
        payload: users
    }
}

//this function signals that the request to obtain user information from the specified server has failed
export const fetchUsersFailure = () => {
    return {
        type: FETCH_USERS_FAILURE,
        payload: console.error()
    }
}

//the function of the complete process of obtaining user information from the specified server
//and sending it to the store
export const fetchUsers = () => {
    return (dispatch) => {
        //we begin the specified process
        dispatch(fetchUsersRequest);
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(response => {
            //information about users from the specified server has been received
            const users = response.data;
            //we send the received information to the store
            dispatch(fetchUsersSuccess(users));
        })
        .catch(error => {
            // the specified process failed we output an error error.message
            const errorMsg = error.message;
            dispatch(fetchUsersFailure(errorMsg));
        })
    }
}