//the label of the start of the process of obtaining user information from the server https://jsonplaceholder.typicode.com/users
export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
//the label of successful receipt of user information from the specified server
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
//the label of an unsuccessful receipt of user information from the specified server
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';