import React, {useEffect} from 'react';
import {connect} from 'react-redux';
//we pull out the function of fully obtaining information about users
import { fetchUsers } from '../redux';

function UserContainer ({
    //received data about users
    userData, dfetchUsers
}) {
    useEffect(() => {
        //we signal the process of fully obtaining information about users in hook useEffect
        dfetchUsers();
    }, [dfetchUsers]);

    return userData.loading ? (
        //the process of obtaining information about users is underway
        <h2>Loading</h2>
    ) : userData.error ? (
        //the process of obtaining user information failed
        <h2>userData.error</h2>
    ) : (
        //we display the received information about users
        <div>
            <h2>User List</h2>
            <div>
                {
                    userData &&
                    userData.users &&
                    userData.users.map(user => <p>{user.name}</p>)
                }
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        //we collect data about users into userData from the store
        userData: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        //this function refers to the hook useEffect
        dfetchUsers: () => dispatch(fetchUsers())
    }
}

//using the function connect we communicate with store and we get information from it in the form of userData
export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);