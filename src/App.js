import React from 'react';
import { Provider } from 'react-redux';
//Pulling out the user data storage
import store from './redux/store';
import './App.css';
//Pulling out the project page
import UserContainer from './components/UserContainer';

function App() {
  return (
    //User Data Storage
    <Provider store={store}>
      <div className="App">
        {/*Showing the project page */}
        <UserContainer/>
      </div>
    </Provider>
  );
}

export default App;
